def print_name(name: str) -> str:
    print(name)


print_name("Maciej")


def print_sum(a: int, b: int, c: int) -> int:
    print(a + b + c)


print_sum(1, 2, 3)


def print_hello_world() -> str:
    print("Hello World")


print_hello_world()


def print_sub(a: int, b: int) -> int:
    print(a - b)


print_sub(5, 3)


class Soap:
    pass


def get_soap() -> Soap:
    return Soap()


print(isinstance(get_soap(), Soap))
